echo "Setting Microservice Name..."
$env:SERVICE_NAME=echo "import os;v=os.path.basename(os.path.dirname(os.path.abspath(__file__))).replace('-','_').replace(' ','_').upper();print(v)" | python
echo "Stopping Service if exists/started..."
Stop-Service MS-$env:SERVICE_NAME; echo ""
echo "Creating Service Folder..."
If (!(Test-Path "c:\Program Files\microservices\$env:SERVICE_NAME")) {New-Item -Path "c:\Program Files\microservices\$env:SERVICE_NAME" -ItemType Directory}
echo "Removing Previous Version..."
Remove-Item -Force -Recurse -Path "c:\Program Files\microservices\$env:SERVICE_NAME\*"
echo "Copying Service Files..."
Copy-Item .\* -Destination "c:\Program Files\microservices\$env:SERVICE_NAME" -recurse -Force
cd "c:\Program Files\microservices\$env:SERVICE_NAME"
echo "Activating Service Environment..."
venv\scripts\activate
echo "Creating Database if not exists..."
# sqlcmd -S $env:SQLSERVER_HOST -Q "IF NOT EXISTS (SELECT 1 FROM sys.databases WHERE name = N'$env:SERVICE_NAME') BEGIN CREATE DATABASE [$env:SERVICE_NAME] END;"
# sqlcmd -S $env:SQLSERVER_HOST -Q "IF NOT EXISTS (SELECT 1 FROM sys.databases WHERE name = N'other') BEGIN CREATE DATABASE [other] END;"
echo "Applying Database Changes..."
python manage.py migrate
python manage.py loaddata fixture.json
echo "Copying Static Web Files..."
python manage.py collectstatic --noinput
echo "Creating Admin User..."
echo "from django.contrib.auth import get_user_model; User = get_user_model(); User.objects.filter(username='admin').exists() or User.objects.create_superuser('admin', 'admin@domain.com', 'P@ssw0rd')" | python manage.py shell
echo "Copying pywintypes38 dll..."
cp venv\Lib\site-packages\pywin32_system32\pywintypes38.dll venv\Lib\site-packages\win32\pywintypes38.dll
echo "Installing Service..."
venv\Scripts\python.exe windows_service.py --username $env:MICROSERVICE_ACCOUNT --password $env:MICROSERVICE_PASSWORD --startup auto install
echo "Starting Service..."
venv\Scripts\python.exe windows_service.py start
