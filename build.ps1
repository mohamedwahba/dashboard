echo "Creating Virtual Environment..."
python -m venv venv
echo "Activating Virtual Environment..."
venv\scripts\activate
echo "Installing rcssmin..."
pip install rcssmin --install-option="--without-c-extensions"
echo "Installing win32 package..."
pip install pywin32
echo "Installing Service Dependancies..."
pip install -r requirements.txt