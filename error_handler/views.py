from django.shortcuts import render
import logging
from django.contrib import messages

logger = logging.getLogger(__name__)


def handler404(request, exception):
    logger.exception(f'Page not found error 404 {exception}')
    context = {}
    response = render(request, "errors/404.html", context=context)
    response.status_code = 404
    return response


def handler500(request):
    logger.exception('Internal Server Error 500')
    context = {}
    response = render(request, "errors/500.html", context=context)
    response.status_code = 500
    return response
