from django.shortcuts import render
from django.contrib.auth.decorators import login_required


@login_required(login_url="/login/")
def home(request):
    egp_balance = 90879.8
    usd_balance = 438.23
    exchange_rate = 15.97
    convenience_fees_rate = 1.27
    convenience_fees = round(egp_balance * convenience_fees_rate / 100, 2)
    egp_equivalent = round(usd_balance * exchange_rate, 2)
    total_balance = round(egp_balance + egp_equivalent, 2)
    total_payment = round(total_balance + convenience_fees, 2)
    context = {
        'title': 'My Balance',
        'name': 'Mohamed Ali',
        'university_id': '900181298',
        'email': 'mohamed.ali@aucegypt.edu',
        'egp_balance': egp_balance,
        'usd_balance': usd_balance,
        'egp_equivalent': egp_equivalent,
        'convenience_fees': convenience_fees,
        'total_balance': total_balance,
        'total_payment': total_payment,
    }
    return render(request, 'app/home.html', context)
