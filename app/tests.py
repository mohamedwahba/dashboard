from django.test import TestCase
import requests


class Integration(TestCase):
    def test_event_receive(self):
        pass


from project.settings import SERVICE_PORT


class Deployment(TestCase):
    def test_service_working(self):
        r = requests.get('http://localhost:' + str(SERVICE_PORT) + '/admin')
        self.assertEqual(r.status_code, 200)
