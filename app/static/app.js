Vue.use(VueMaterial.default)
var app = new Vue({
    el: '#app',
    delimiters: ['[[', ']]'],
    name: 'PersistentMini',
    data: () => ({
        menuVisible: false,
        payOnlineHidden: false
    }),
    methods: {
        toggleMenu() {
            this.menuVisible = !this.menuVisible
        },
        payOnlineClicked() {
            document.getElementById('pay-now').elmnt.scrollIntoView();
            this.payOnlineHidden = true;

        }
    }
})