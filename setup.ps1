echo "========= Build ==========="
./build.ps1
echo "========= Test ==========="
./test.ps1
echo "========= Deploy ==========="
./deploy.ps1
echo "========= Health Check ==========="
./healthcheck.ps1