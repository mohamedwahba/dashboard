import os
import sys

service_directory = os.path.dirname(__file__)
source_directory = os.path.abspath(service_directory)
os.chdir(source_directory)
venv_base = os.path.abspath(os.path.join(source_directory, "venv"))
sys.path.append(".")
old_os_path = os.environ['PATH']
os.environ['PATH'] = os.path.join(venv_base, "Scripts")+ os.pathsep + old_os_path
site_packages = os.path.join(venv_base, "Lib", "site-packages")
prev_sys_path = list(sys.path)
import site
site.addsitedir(site_packages)
sys.real_prefix = sys.prefix
sys.prefix = venv_base
new_sys_path = list()
for item in list(sys.path):
    if item not in prev_sys_path:
        new_sys_path.append(item)
        sys.path.remove(item)
sys.path[:0] = new_sys_path

import win32serviceutil
import win32service
import win32event
import servicemanager
import socket
import time
import logging
from waitress_server import WaitressServer
import sys
from project.settings import SERVICE_PORT

service_name = os.path.basename(os.path.dirname(os.path.abspath(__file__))).replace('-','_').replace(' ','_').upper()

logging.basicConfig(
    filename = service_name + '.log',
    level = logging.DEBUG, 
    format = '['+service_name+'] %(levelname)-7.7s %(message)s'
)

class UacSvc (win32serviceutil.ServiceFramework):
    _svc_name_ = "MS-"+service_name
    _svc_display_name_ = "Microservice - Port: "+str(SERVICE_PORT)+" - " + service_name
    _svc_description_ = "A Service to manage " + service_name + '.'
    
    


    server = WaitressServer('127.0.0.1',SERVICE_PORT)

    def __init__(self,args):
        win32serviceutil.ServiceFramework.__init__(self,args)
        self.stop_event = win32event.CreateEvent(None,0,0,None)
        socket.setdefaulttimeout(60)
        self.stop_requested = False

    def SvcStop(self):
        self.ReportServiceStatus(win32service.SERVICE_STOP_PENDING)
        win32event.SetEvent(self.stop_event)
        logging.info('Stopping service ...')
        self.stop_requested = True
        self.server.stop_service()

    def SvcDoRun(self):
        servicemanager.LogMsg(
            servicemanager.EVENTLOG_INFORMATION_TYPE,
            servicemanager.PYS_SERVICE_STARTED,
            (self._svc_name_,'')
        )
        try:
            self.main()
        except Exception as e:
            logging.info(str(e))

    def main(self):
        logging.info(' ** '+service_name+' Service Started ** ')
        self.server.run()
        return

if __name__ == '__main__':
    win32serviceutil.HandleCommandLine(UacSvc)