
from waitress.server import create_server
from project import wsgi

class WaitressServer:

	def __init__(self, host, port):
		self.server = create_server(wsgi.application, host=host, port=port)

    # call this method from your service to start the Waitress server
	def run(self):
		self.server.run()

    # call this method from the services stop method
	def stop_service(self):
		self.server.close()